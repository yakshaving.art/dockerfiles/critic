FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master

# This is a multiarch image
ARG TARGETOS
ARG TARGETARCH

LABEL maintaner="Ilya A. Frolov <if+gitlab@solas.is>" \
      description="Image we use as a builder image for packer images"

SHELL ["/bin/bash", "-EeufCo", "pipefail", "-c"]

# hadolint ignore=DL3013,DL3018
RUN \
	# We bind-mount scripts and tests so that we don't pollute the end image
	# NOTE: instead of mounting separate dirs, we mount both so that we don't have
	# empty /mnt/scripts and /mnt/tests directory leftovers in the image :ocd:
	--mount=type=bind,source=.gitlab.d/ci,target=/mnt,readonly \
	# We also bind-mount goss and trivy binaries from the docker-builder image
	--mount=type=bind,source=goss,target=/bin/goss,readonly \
	--mount=type=bind,source=trivy,target=/bin/trivy,readonly \
	# And we also bind-mount .trivyignore file
	--mount=type=bind,source=.trivyignore,target=/.trivyignore,readonly \
	# Main flow below
	apk add --no-cache \
		bats \
		coreutils \
		findutils \
		grep \
	# install binaries from hoarder, multiarch
	&& bash /usr/local/bin/unhoard.sh goss		"${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh hadolint	"${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh hcl2json	"${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh shellcheck	"${TARGETOS}" "${TARGETARCH}" \
	# install yamllint and yq from pip (we need python's yq, not golang implementation)
	&& ( apk -qq --no-cache add python3 py3-pip \
		&& pip3 install --break-system-packages --no-cache-dir yamllint yq \
		&& apk -qq del py3-pip py-setuptools \
		&& rm -rf /root/.cache ) \
	# fix permissions
	&& chmod -R 0755 /usr/local/bin \
	# at this point, the _build_ is done, and we proceed to run _tests_ from within the image,
	# in order to abort the whole thing if they fail, and never push anything unsafe to the registry
	# these stages should be the last, and they should also be self-contained, i.e. do not install
	# any dependencies to not pollute the end image
	&& bash /mnt/scripts/skkrty_inside_build.sh \
	&& bash /mnt/scripts/tests_inside_build.sh \
	&& echo "done"
