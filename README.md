[![pipeline status](https://gitlab.com/yakshaving.art/dockerfiles/base/badges/master/pipeline.svg)](https://gitlab.com/yakshaving.art/dockerfiles/base/-/commits/master)
# critic

The guy with enough tools at his disposal to tell exactly how much your code stinks.

This image is multiarch, see os/architectures we build in .gitlab-ci.yml

# Tools installed:
 - bats
 - goss
 - hadolint
 - json2hcl
 - shellcheck
 - yamllint from pip
 - yq from pip
